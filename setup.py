import setuptools


setuptools.setup(
    name="inventory",
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="DataBricks test task package",
    long_description="DataBricks test task package",
    packages=setuptools.find_packages(exclude=("tests",),),
    test_suite="tests",
    install_requires=["pyspark>=2.3.0", "mock;python_version<'3.3'"]
)
