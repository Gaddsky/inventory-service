import unittest
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.utils import AnalysisException
from pyspark.conf import SparkConf
import os.path

try:
    import unittest.mock as mock
except ImportError:
    import mock


from inventory.inventory_service import InventoryService

RESOURCES_FOLDER_NAME = "sample_data"

# name of the directory, where `sample_data` is located
dirname = os.path.dirname(os.path.abspath(__file__))

RESOURCES_FOLDER_PATH = os.path.join(dirname, RESOURCES_FOLDER_NAME)

WRONG_RESOURCE_FOLDER = os.path.join(dirname, "wrong-path")


class TestInventoryService(unittest.TestCase):
    inv_serv = None

    def setUp(self):
        self.spark = SparkSession.builder.config(conf=SparkConf()).getOrCreate()
        self.inv_serv = InventoryService("", "", "", self.spark)

    def test_get_vin_data_returns_correct_type(self):
        self.inv_serv._s3_folder_path = RESOURCES_FOLDER_PATH

        data = self.inv_serv.get_vin_data()
        self.assertIsInstance(data, DataFrame, "Result is not DataFrame instance")

    def test_get_vin_data_rise_exception_in_case_wrong_path(self):
        self.inv_serv._s3_folder_path = WRONG_RESOURCE_FOLDER

        with self.assertRaises(AnalysisException) as context:
            self.inv_serv.get_vin_data()
        self.assertTrue('Path does not exist' in context.exception.desc)

    def test_get_vin_data_check_result_columns(self):
        self.inv_serv._s3_folder_path = RESOURCES_FOLDER_PATH

        expected_columns = {'dealer_name', 'dealership_id', 'make', 'model', 'vins'}

        data = self.inv_serv.get_vin_data()
        actual_columns = set(data.columns)
        self.assertEqual(len(data.columns), len(actual_columns), "Result data has duplicating columns")
        self.assertSetEqual(actual_columns, expected_columns, "Result data has wrong columns")

    def test_get_vin_data_results_check(self):
        self.inv_serv._s3_folder_path = RESOURCES_FOLDER_PATH

        expected_results = [
            ('dealer name 5', 1005, 'Manufacturer3', 'Model3-2', [10000019]),
            ('dealer name 4', 1004, 'Manufacturer2', 'Model2-1', [10000013]),
            ('dealer name 5', 1005, 'Manufacturer3', 'Model3-1', [10000018]),
            ('dealer name 5', 1005, 'Manufacturer2', 'Model2-2', [10000016, 10000014, 10000015]),
            ('dealer name 1', 1001, 'Manufacturer1', 'Model1-1', [10000001]),
            ('dealer name 4', 1004, 'Manufacturer1', 'Model1-1', [10000012, 10000011]),
            ('dealer name 2', 1002, 'Manufacturer2', 'Model2-1', [10000004, 10000003])
        ]

        expected_data_frame = self.spark.createDataFrame(expected_results,
                                                         ("dealer_name", "dealership_id", "make", "model", "vins"))
        data = self.inv_serv.get_vin_data()

        self.assertEqual(data.count(), expected_data_frame.count(), "Expected and actual data should have equal sizes")

        # expect the difference between expected results and actual data is zero
        self.assertEqual(data.subtract(expected_data_frame).count(), 0)

    def test_get_vin_data_results_check_with_mock(self):
        catalog_data = [(1, "d1", 11), (2, "d2", 22)]
        catalog_mock = self.spark.createDataFrame(catalog_data, ("dealership_id", "dealer_name", "franchise_id"))

        feed_data = [
            (101, 2010, "m1", "md1", 11),
            (102, 2009, "m1", "md1", 11),
            (103, 2011, "m2", "md2", 22)
        ]
        feed_mock = self.spark.createDataFrame(feed_data, ("vin", "year", "make", "model", "franchise_id"))

        expected_results = [
            ('d2', 2, 'm2', 'md2', [103]),
            ('d1', 1, 'm1', 'md1', [101])
        ]
        expected_data_frame = self.spark.createDataFrame(expected_results,
                                                         ("dealer_name", "dealership_id", "make", "model", "vins"))

        with mock.patch.object(self.inv_serv, "_get_catalog_df", mock.Mock(return_value=catalog_mock)), \
             mock.patch.object(self.inv_serv, "_get_feed_df", mock.Mock(return_value=feed_mock)):
            data = self.inv_serv.get_vin_data()

            self.inv_serv._get_catalog_df.assert_called_once_with("catalog")
            self.inv_serv._get_feed_df.assert_called_once_with("feed")

            self.assertEqual(data.count(), expected_data_frame.count(),
                             "Expected and actual data should have equal sizes")

            # expect the difference between expected results and actual data is zero
            self.assertEqual(data.subtract(expected_data_frame).count(), 0, "Content of DataFrames are different")


if __name__ == '__main__':
    unittest.main()
