import os.path

from pyspark import SparkConf
from pyspark.sql import functions as f, SparkSession


class InventoryService:
    """
    Usage:
        1. Create instance of InventoryService providing AWS credentials and AWS basket name
        2. Call get_vin_data method of the instance
    """

    def __init__(self, access_key, secret_key, aws_bucket_name, spark_instance, *parent_folders):
        self._spark = spark_instance

        encoded_secret_key = secret_key.replace("/", "%2F")
        s3_path = 's3a://%s:%s@%s' % (access_key, encoded_secret_key, aws_bucket_name)
        self._s3_folder_path = os.path.join(s3_path, *parent_folders)

    def _get_dataframe(self, delimiter, filetype, folder):
        folder_path = os.path.join(self._s3_folder_path, folder)
        return self._spark.read.format('csv')\
            .options(header='true', inferSchema='true', delimiter=delimiter)\
            .load(folder_path + "/*.{}".format(filetype))

    def _get_catalog_df(self, folder, delimiter=",", file_type="csv"):
        return self._get_dataframe(delimiter, file_type, folder)

    def _get_feed_df(self, folder, delimiter="\t", file_type="tsv"):
        return self._get_dataframe(delimiter, file_type, folder)

    def get_vin_data(self, year=2010):
        group_by_fields = "dealer_name", "dealership_id", "make", "model"

        catalog_df = self._get_catalog_df("catalog")
        feed_df = self._get_feed_df("feed")
        joined_df = feed_df.join(catalog_df, "franchise_id").dropDuplicates().filter("year >= {}".format(year))
        return joined_df.groupBy(*group_by_fields).agg(f.collect_set("vin").alias("vins"))


if __name__ == "__main__":
    # some code for standalone local run
    from aws_settings import *

    spark = SparkSession.builder.config(conf=SparkConf()).getOrCreate()
    inv_serv = InventoryService(ACCESS_KEY, SECRET_KEY, AWS_BUCKET_NAME, spark, "inventory", "pre-processed")
    data = inv_serv.get_vin_data()
