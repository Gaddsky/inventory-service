# Databricks notebook source
# MAGIC %run propensity/inventory/settings

# COMMAND ----------

from inventory.inventory_service import InventoryService

inv_serv = InventoryService(ACCESS_KEY, SECRET_KEY, AWS_BUCKET_NAME, spark, "inventory", "pre-processed")
result = inv_serv.get_vin_data()

display(result)

# COMMAND ----------


